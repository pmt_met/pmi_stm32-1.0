/* Define to prevent recursive inclusion*/
#ifndef __HPDRAGON_UART_H
#define __HPDRAGON_H

#include "stm32f10x_usart.h"

#ifdef __GNUC__
	/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
	set to 'Yes') calls __io_putchar() */
	#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
	#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

#define HPUart					USART1

void UART_InitPrintf(void);

#endif /* __HPDRAGON_UART_H */
