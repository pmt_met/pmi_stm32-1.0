/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "GLCD.h"
#include "main.h"
#include "diskio.h"
#include "bmp.h"
#include "ff.h"
#include <stdio.h>
#include <string.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* SD_Power */
#define SD_PowerOn()        GPIO_ResetBits(GPIOC, GPIO_Pin_4)
#define SD_PowerOff()	      GPIO_SetBits(GPIOC, GPIO_Pin_4)

/* Private variables ---------------------------------------------------------*/
	static char* errstr[] = {
		"OK", "DISK_ERR", "INT_ERR", "NOT_READY", "NO_FILE", "NO_PATH",
		"INVALID_NAME", "DENIED", "EXIST", "INVALID_OBJECT", "WRITE_PROTECTED",
		"INVALID_DRIVE", "NOT_ENABLED", "NO_FILE_SYSTEM", "MKFS_ABORTED", "TIMEOUT",
		"LOCKED", "NOT_ENOUGH_CORE", "TOO_MANY_OPEN_FILES"};

BYTE wait = 0;
char sbuff[480]; // String buffer

DWORD AccSize;			/* Work register for fs command */
WORD AccFiles, AccDirs;
FILINFO Finfo;
char Lfname[512]; // Long file name buffer

BYTE sdbuff[512]; // Working buffer

FATFS Fatfs;			/* File system object */
		
__IO uint32_t TimingDelay;
/* Private function prototypes -----------------------------------------------*/
void GPIO_init(void);
void EXTI_init(void);
void RTC_firstinit(void);
void RTC_init(void);
void Time_Adjust(void);
void printFR(FRESULT);
/* Private functions ---------------------------------------------------------*/

void Delay(__IO uint32_t nTime) { 
	TimingDelay = nTime/10 + 1;
	while(TimingDelay != 0);
}
char* get_filename_ext(char *filename) {
    char *dot = strrchr(filename, '.');
    if(!dot || dot == filename) return "";
    return dot + 1;
}
static FRESULT scan_files (char* path)	{	/* Pointer to the working buffer with start path */
	DIR dirs; // Dir object
	FRESULT res; // FF result
	int i;
	char *fn; // File name

	res = f_opendir(&dirs, path);
	if (res == FR_OK) {
		i = strlen(path);
		while (((res = f_readdir(&dirs, &Finfo)) == FR_OK) && Finfo.fname[0]) {
			if (_FS_RPATH && Finfo.fname[0] == '.') continue;
#if _USE_LFN
			fn = *Finfo.lfname ? Finfo.lfname : Finfo.fname;
#else
			fn = Finfo.fname;
#endif
			if (Finfo.fattrib & AM_DIR) {
				AccDirs++;
				*(path+i) = '/'; strcpy(path+i+1, fn); 
				res = scan_files(path); // Scan in the child dir
				*(path+i) = '\0'; // Continue scaning in this dir
				if (res != FR_OK) break;
			} else {
				AccFiles++;
				AccSize += Finfo.fsize;
			}
		}
	}

	return res;
}


/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void) {
	FATFS* fs;
	DIR dir; // Dir object
	FIL fil;	// File object
	FRESULT res; // FF result
	char *fn; // File name
	unsigned long dat = 0;
	
	Finfo.lfname = Lfname;
	Finfo.lfsize = sizeof(Lfname);
	
	GPIO_init();	
	if (SysTick_Config(720)) while (1);
	
  GLCD_init();
	GLCD_clear(Black);
	GLCD_println("GLCD is ready...");	
	RTC_init();
	
	// Physical Init
	SD_init();	
	EXTI_init();
	
	// Logical Init
	GLCD_print("Mount drive 0: "); printFR(f_mount(0, &Fatfs));
	// Get drive status
	GLCD_print("Get drive status: "); 
	while(f_getfree("0:", (DWORD*)&dat, &fs)) ;
	GLCD_println("OK");
	AccSize = AccFiles = AccDirs = 0;
	GLCD_print("Get folder statistics: "); printFR(scan_files("/"));
	sprintf(sbuff, "FAT type = %u\nBytes/Cluster = %lu\nNumber of FATs = %u\n"
							 "Root DIR entries = %u\nSectors/FAT = %lu\nNumber of clusters = %lu\n"
							 "FAT start (lba) = %lu\nDIR start (lba,clustor) = %lu\nData start (lba) = %lu\n"
							 "%u files, %lu bytes.\n%u folders.\n"
							 "%lu KB total disk space.\n%lu KB available.\n"
							 "--------------------------------",
						fs->fs_type, (DWORD)fs->csize * 512, fs->n_fats,
						fs->n_rootdir, fs->fsize, fs->n_fatent - 2,
						fs->fatbase, fs->dirbase, fs->database,
						AccFiles, AccSize, AccDirs,
						(fs->n_fatent - 2) * (fs->csize / 2), dat * (fs->csize / 2));
	GLCD_print((uint8_t*)sbuff);	
	
	// Set current drive and dir
	f_chdrive(0);
	f_chdir("/");
	
	// Scan for .txt file
	res = f_opendir(&dir, ".");
	if (res == FR_OK) {
		while ((res = f_readdir(&dir, &Finfo)) == FR_OK) {
			if (Finfo.fname[0] == 0) {
				f_readdir(&dir, 0);
				continue;
			}
			if (_FS_RPATH && Finfo.fname[0] == '.') continue;
#if _USE_LFN
			fn = *Finfo.lfname ? Finfo.lfname : Finfo.fname;
#else
			fn = Finfo.fname;
#endif
			if (!(Finfo.fattrib & AM_DIR)) {
				/*if(!strcmp(get_filename_ext(fn), "txt")) {
					GLCD_print("Found "); GLCD_println((uint8_t*)fn);
					GLCD_print("Opening: "); printFR(f_open(&fil, fn, FA_READ | FA_OPEN_EXISTING));
					GLCD_print("Reading: "); printFR(f_read(&fil, sdbuff, sizeof(sdbuff), (UINT*)&dat));
					GLCD_println(sdbuff);
					sprintf(sbuff, "Byte read: %u", dat);
					GLCD_println((BYTE*)sbuff);
					GLCD_print("Closing: "); printFR(f_close(&fil));
				}*/
				if(!strcmp(get_filename_ext(fn), "bmp")) {
					GLCD_print("Found "); GLCD_println((uint8_t*)fn);
					GLCD_print("Opening: "); printFR(f_open(&fil, fn, FA_READ | FA_OPEN_EXISTING));
					GLCD_println("Reading...");
					if(!openBMP(&fil)) {
						wait = 1;
						while (wait) ;
					} else {
						GLCD_println("Error!");
					}
				}
			}
		}
	}

  while (1) {
  }
}

void GPIO_init(void) {
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE, ENABLE);
	
	//EXTI
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	//SD_PWR
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
}

void EXTI_init(void) {
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef  NVIC_InitStructure;
	
	// EXTI
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource13);

	EXTI_InitStructure.EXTI_Line = EXTI_Line13;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void SD_init(void) {
	SD_PowerOff();
	GLCD_print("SD card status: ");
	if(GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_14) == Bit_RESET) {
		GLCD_println("Detected");
		SD_PowerOn();
		power();
		//GLCD_print("SD card init: ");
		/*if (!disk_initialize(0)) {
			GLCD_println("OK");
			sd_cd = 1;
		} else {
			GLCD_println("Error");
			sd_cd = 0;
		}*/
	}
	else {
		GLCD_println("Not found");
		SD_PowerOff();
	}
}

void RTC_init(void) {	
	DWORD time;
	uint32_t rtcyear, rtcmonth, rtcday, rtchour, rtcmin, rtcsec;
	
	if (BKP_ReadBackupRegister(BKP_DR1) != 0xA5A5)
  {
    /* Backup data register value is not correct or not yet programmed (when
       the first time the program is executed) */  
    /* RTC Configuration */
    RTC_firstinit(); 
    /* Adjust time by values entered by the user on the hyperterminal */
    Time_Adjust(); 
    BKP_WriteBackupRegister(BKP_DR1, 0xA5A5);		
		GLCD_println("RTC first configured...");
  }
  else
  {
		/* Wait for RTC registers synchronization */
		RTC_WaitForSynchro();
		/* Enable the RTC Second */
		RTC_ITConfig(RTC_IT_SEC, ENABLE);
    /* Wait until last write operation on RTC registers has finished */
    RTC_WaitForLastTask();		
		GLCD_print("RTC: ");
		
		time = get_fattime();
		rtcyear = ((time >> 25) & 0x7F) + 1980;
		rtcmonth = (time >> 21) & 0x0F;
		rtcday = (time >> 16) & 0x1F;
		rtchour = (time >> 11) & 0x1F;
		rtcmin = (time >> 5) & 0x3F;
		rtcsec = (time << 1) & 0x3F;
		
		sprintf(sbuff, "%u/%u/%u %u:%u:%u", rtcday, rtcmonth, rtcyear, rtchour, rtcmin, rtcsec);
		GLCD_println((uint8_t*)sbuff);
  }
}

void RTC_firstinit(void) {
  /* Enable PWR and BKP clocks */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE); 
  /* Allow access to BKP Domain */
  PWR_BackupAccessCmd(ENABLE); 
  /* Reset Backup Domain */
  BKP_DeInit(); 
  /* Enable LSE */
  RCC_LSEConfig(RCC_LSE_ON);
  /* Wait till LSE is ready */
  while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET) {} 
  /* Select LSE as RTC Clock Source */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE); 
  /* Enable RTC Clock */
  RCC_RTCCLKCmd(ENABLE);
  /* Wait for RTC registers synchronization */
  RTC_WaitForSynchro();
  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
  /* Enable the RTC Second */
  RTC_ITConfig(RTC_IT_SEC, ENABLE); 
  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
  /* Set RTC prescaler: set RTC period to 1sec */
  RTC_SetPrescaler(32767); /* RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) */
  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
}

void Time_Adjust(void) {
  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
  /* Change the current time */
  RTC_SetCounter(12061*86400 + 17*3600 + 28*60);
  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
}

DWORD get_fattime(void) {
	uint32_t rtc, rtcyear, rtcmonth, rtcday, rtchour, rtcmin, rtcsec;
	const uint8_t dims[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	uint32_t dim = dims[0];
	uint32_t diy = 366;
	uint32_t totalday;
	rtc = RTC_GetCounter();
	rtcsec = rtc % 60;
	rtcmin = (rtc % 3600) / 60;
	rtchour = (rtc % 86400) / 3600;
	totalday = rtc / 86400;
	
	rtcyear = 0;
	while (totalday >= diy) {
		totalday -= diy;
		rtcyear++;
		if (rtcyear % 4 == 0) diy = 366;
		else diy = 365;
	}
	
	rtcmonth = 1;
	while (totalday >= dim) {
		totalday -= dim;
		dim = dims[rtcmonth];
		rtcmonth++;
		if ((rtcmonth == 2) && (rtcyear % 4 == 0)) dim++;
	}
	
	rtcday = totalday + 1;
	
	return  (((DWORD)rtcyear << 25)
			| ((DWORD)rtcmonth << 21)
			| ((DWORD)rtcday << 16)
			| (WORD)(rtchour << 11)
			| (WORD)(rtcmin << 5)
			| (WORD)(rtcsec >> 1));	
}

void printFR(FRESULT fr) {
		GLCD_println((uint8_t*)errstr[fr]);
}
void nextBmp(void) {
	wait = 0;
}


#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif
