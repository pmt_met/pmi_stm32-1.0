/*----------------------------------------------------------------------------
 * Name:    GLCD.c
 * Purpose: ILI9341 GLCD driver
 * Version: V1.00
 * Note(s):
 *----------------------------------------------------------------------------
 * This file is part of the uVision/ARM development tools.
 * This software may only be used under the terms of a valid, current,
 * end user licence from KEIL for a compatible version of KEIL software
 * development tools. Nothing else gives you the right to use this software.
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 * Copyright (c) 2009 Keil - An ARM Company. All rights reserved.
 *----------------------------------------------------------------------------*/

#include <stm32f10x_cl.h>
#include <stdio.h>
#include "GLCD.h"
#include "terminal.h"

/* SPI_SR - bit definitions. */
#define RXNE    0x01
#define TXE     0x02
#define BSY     0x80
										 
/*********************** Hardware specific configuration **********************/

/*------------------------- Speed dependant settings -------------------------*/

/* If processor works on high frequency delay has to be increased, it can be 
   increased by factor 2^N by this constant                                   */
#define DELAY_2N    60

/*---------------------- Graphic LCD size definitions ------------------------*/

#define WIDTH       320                 /* Screen Width (in pixels)           */
#define HEIGHT      240                 /* Screen Hight (in pixels)           */
#define BPP         16                  /* Bits per pixel                     */
#define BYPP        ((BPP+7)/8)         /* Bytes per pixel                    */

/*--------------- Graphic LCD interface hardware definitions -----------------*/

#define SPI_START   (0x70)              /* Start byte for SPI transfer        */
#define SPI_RD      (0x01)              /* WR bit 1 within start              */
#define SPI_WR      (0x00)              /* WR bit 0 within start              */
#define SPI_DATA    (0x02)              /* RS bit 1 within start byte         */
#define SPI_INDEX   (0x00)              /* RS bit 0 within start byte         */
 
/*---------------------------- Global variables ------------------------------*/

unsigned char lines[480];
unsigned char c_ofs = 0;

/******************************************************************************/

/************************ Local auxiliary functions ***************************/

/*******************************************************************************
* Delay in while loop cycles                                                   *
*   Parameter:    cnt:    number of while cycles to delay                      *
*   Return:                                                                    *
*******************************************************************************/

static void delay (unsigned int cnt) {

  unsigned int i,j;  

  for (i=1;i<=cnt; i++)
  	for (j=1;j<10000;j++);
}

/*******************************************************************************
* Write data (16 bit) to LCD controller                                        *
*   Parameter:    c:      data to be written                                   *
*   Return:                                                                    *
*******************************************************************************/

static __inline void wr_dat (unsigned short c) {
	RS
	CS
	RDN
	WR
	GPIOE->ODR = c;
	WRN
	CSN
}

/*******************************************************************************
* Write command to LCD controller                                              *
*   Parameter:    c:      command to be written                                *
*   Return:                                                                    *
*******************************************************************************/

static __inline void wr_cmd (unsigned char c) {
	RSN
	CS
	RDN
	WR
	GPIOE->ODR = c;
	WRN
	CSN
}

/*******************************************************************************
* Read data from LCD controller                                                *
*   Parameter:                                                                 *
*   Return:               read data                                            *
*******************************************************************************/

static __inline unsigned short rd_dat (void) {
  unsigned short val = 0;

	GPIOE->CRH = 0x44444444; // Floating input
	GPIOE->CRL = 0x44444444; // Floating input
	
	RS
	WRN
	CS
	RD
	RDN
	val = GPIOE->IDR;
	CSN
	
	GPIOE->CRH = 0x33333333; // output push-pull 
	GPIOE->CRL = 0x33333333; // output push-pull

	return val;  
}

/*******************************************************************************
* Read the Graphic LCD module ID code                                          *
*   Parameter:                                                                 *
*   Return:                                                                    *
*******************************************************************************/
static void ili9341_read_id (void) {

	unsigned short id0, id1, id2, id3;

	wr_cmd(0xd3);  			 // Read ID4
	id0 = rd_dat();      // read dummy data
	id1 = rd_dat();	// 00
	id2 = rd_dat(); // 93
	id3 = rd_dat(); // 41
}

static void ili9341_set_window (unsigned short col_l,   
                                unsigned short col_h,	 // col_l <= col_h < 320 --> x
								                unsigned short row_l,	 // row_l <= row_h < 240 --> y
								                unsigned short row_h) {

	wr_cmd(0x2A);              // Column address set  (column max = 320)
	
	wr_dat((col_l>>8)& 0xFF);  // Column high byte (lower column)
	wr_dat(col_l & 0xFF);      // Column low byte  (lower column)

	wr_dat((col_h>>8)& 0xFF);  // Column high byte (upper column)
	wr_dat(col_h & 0xFF);      // Column low byte  (upper column)
    
		
	wr_cmd(0x2B);              // Page address set	  (row max = 240)

	wr_dat((row_l>>8)& 0xFF);  // Row high byte   (lower row)
	wr_dat(row_l & 0xFF);      // Row low byte    (lower row)

	wr_dat((row_h>>8)& 0xFF);  // Row high byte   (upper row)
	wr_dat(row_h & 0xFF);      // Row low byte    (upper row)
		  
	wr_cmd(0x2C);							 // Memory Write

}


 
/************************ Exported functions **********************************/

/*******************************************************************************
* Initialize the Graphic LCD controller                                        *
*   Parameter:                                                                 *
*   Return:                                                                    *
*******************************************************************************/

void GLCD_init (void) {
													   
// Hardware interface
// nCS         PC6
// RS          PD13
// nWR         PD14
// nRD         PD15
// DATA[15:0]  PE[15:0]

  /* Enable clock for GPIOA,B,C,D,E AFIO and SPI3. */
  RCC->APB2ENR |= 0x0000007D;

  /* PE output set to high. */
  GPIOE->CRL = 0x33333333;	    // output push-pull
  GPIOE->CRH = 0x33333333;		// output push-pull

  GPIOC->CRL &= 0xf0ffffff;	    // PC6 output push-pull 50MHz
  GPIOC->CRL |= 0x03000000;

  GPIOD->CRH &= 0x000fffff;	    //PD13,14,15 output push-pull 50MHz
  GPIOD->CRH |= 0x33300000;
	
	GPIOD->CRL &= 0x0fffffff;	    //PD7 output push-pull 50MHz
  GPIOD->CRL |= 0x30000000;

  CSN
	RS
	WRN
	RDN
	BL

  delay(10);                    /* Delay 50 ms                        */
	
	ili9341_read_id();    

	wr_cmd(0x11);	// Sleep Out
	
	delay(20);

	wr_cmd(0xC0); // Power Control 1
	wr_dat(0x09);

	wr_cmd(0xC5); // VCOM Control 1
	wr_dat(0x18);
	wr_dat(0x64);

	wr_cmd(0x36); // Memory Access Control
	wr_dat(0xE8);

	wr_cmd(0x3A);	// Pixel Format Set
	wr_dat(0x55);
	
	delay(120);
	wr_cmd(0x29); // Display On
	
	delay(5);

}

void GLCD_putPixel(unsigned int x, unsigned int y, unsigned short color) {
	ili9341_set_window (x, x, y, y);
	wr_dat(color);
}

/*******************************************************************************
* Clear display                                                                *
*   Parameter:    color:  color for clearing display                           *
*   Return:                                                                    *
*******************************************************************************/

void GLCD_clear (unsigned short color) {
  unsigned int   i;
	
	ili9341_set_window(0, WIDTH - 1, 0, HEIGHT - 1);

	for(i = 0; i < (WIDTH*HEIGHT); i++)
	{
		wr_dat(color);
	}   
}

/*******************************************************************************
* Set sector                                                              		 *
*   Parameter:    s:  number of sector								                         *
*									*dat: data
*   Return:                                                                    *
*******************************************************************************/

void GLCD_SetSector (unsigned short s, unsigned short *dat) {
	int i;
	ili9341_set_window(s / 8, s / 8, (s % 8) * 30, (s % 8) * 30 + 29);
	for(i = 0; i < 30; i++)
	{
		wr_dat(dat[i]);
	}
}

/*******************************************************************************
* Set Line	                                                              		 *
*   Parameter:    s:  number of sector								                         *
*									*dat: data																									 *
*   Return:                                                                    *
*******************************************************************************/

void GLCD_SetLine (unsigned short l, unsigned short *dat) {
	int i;
	ili9341_set_window(0, 319, l, l);
	for(i = 0; i < 320; i++)
	{
		wr_dat(dat[i]);
	}
}

/*******************************************************************************
* Draw character on given position (line, coloum                               *
*   Parameter:     x :        horizontal position                              *
*                  y :        vertical position                                *
*                  c :        character							                           *
*   Return:                                                                    *
*******************************************************************************/
void GLCD_drawChar(unsigned int x, unsigned int y, unsigned char c) {
  unsigned int xi = 0;
  unsigned int yi = 0;
	unsigned int offset = (unsigned int)c*9;

  ili9341_set_window(x, x + 9, y, y + 15);

  for(yi = 0; yi < 16; yi++)
  {
    for(xi = 0; xi < 9; xi++)
    {
      if((terminal[offset + xi] & (1 << yi)) == 0) {
         wr_dat(Black);
      } else {
         wr_dat(White);
      }
    }
		wr_dat(Black);
  } 
}

/*******************************************************************************
* Draw character on given position (line, coloum                               *
*   Parameter:     x :        horizontal position                              *
*                  y :        vertical position                                *
*                  c :        character							                           *
*   Return:                                                                    *
*******************************************************************************/
void GLCD_drawString(unsigned int x, unsigned int y, unsigned char *c) {
	unsigned int cx = x;
  for (; *c != 0; c++) {
		if (cx + 9 < 320) 
		{
			GLCD_drawChar(cx, y, *c);
			cx += 10;
		} else break;
	}
}

/*******************************************************************************
* Draw character on given line (line, coloum      			                       *
*   Parameter:     line :     index of line				                             *
*                  offset :   starting point		                               *
*                  c :        character							                           *
*									 len :			length of string																 *
*   Return:                                                                    *
*******************************************************************************/
void GLCD_drawLine(unsigned char line, unsigned char offset, unsigned char *c){
	GLCD_drawString((unsigned int)offset*10, (unsigned int)line*16, c);
}

void GLCD_clearLine(unsigned char line, unsigned char offset, unsigned char len) {
	unsigned int i;
	if (len != 0)
	{
		ili9341_set_window((unsigned short)offset*10, (unsigned short)(offset + len)*10 - 1, (unsigned short)line*16, (unsigned short)line*16 + 15);
		for (i = 0; i < (16*((unsigned short)len*10)); i++) wr_dat(Black);
	}
	else {
		ili9341_set_window((unsigned short)offset*10, 319, (unsigned short)line*16, (unsigned short)line*16 + 15);
		for (i = 0; i < (16*(320 - (unsigned short)offset*10)); i++) wr_dat(Black);
	}	

}

/*******************************************************************************
* Print related functions																											 *
*******************************************************************************/
void GLCD_scrolling(void) {
	int i;
	for (i = 0; i < 448; i++) lines[i] = lines[i+32];
	for (i = 448; i < 480; i++) lines[i] = 0;
}
void GLCD_display(void) {
	int i;
	GLCD_clear(Black);
	for (i = 0; i < 15; i++) GLCD_drawLine(i, 0, lines + i*32);
}

/*******************************************************************************
* Print to screen 																														 *
*******************************************************************************/
void GLCD_print	(unsigned char *c) {
	for (; *c != 0; c++) {
		if (*c != '\n') {
			lines[448 + c_ofs] = *c;
			c_ofs++;
			if (c_ofs >= 32) {
				GLCD_scrolling();
				c_ofs = 0;
			}
		} else {
			GLCD_scrolling();
			c_ofs = 0;
		}
	}
	GLCD_display();
}
void GLCD_println	(unsigned char *c) {
	GLCD_print(c);
	GLCD_scrolling();
	c_ofs = 0;
}
