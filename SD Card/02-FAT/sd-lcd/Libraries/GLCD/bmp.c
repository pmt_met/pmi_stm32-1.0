#include "bmp.h"
#include "glcd.h"
#include "ff.h"

ULONG i, j, dibsiz, imgofs;
ULONG width, height;
char color;
FIL *bmpfil;
UINT dat;

BYTE buff[960]; // Working buffer
USHORT linec[320];
BHEADERTYPE ftype;

USHORT RGB565(BYTE r, BYTE g, BYTE b) {
	return (USHORT)((((USHORT)r & 0xF8) << 8) | (((USHORT)g & 0xFC) << 3) | (b >> 3));
}

BRESULT openBMP(FIL *fil) {
	bmpfil = fil;
	imgofs = 0;
	
	// Read header
	f_read(bmpfil, buff, 14, &dat);
	if (dat == 14) {
		if(!((buff[0] == 0x42) && (buff[1] == 0x4D))) { f_close(bmpfil); return BR_HEADER_ERR; }
		imgofs = (ULONG)buff[10] | ((ULONG)buff[11] << 8) | ((ULONG)buff[12] << 16) | ((ULONG)buff[13] << 24);
	} else { f_close(bmpfil); return BR_HEADER_ERR; }
	
	// Read DIB header
	f_read(bmpfil, buff, 4, &dat);
	dibsiz = (ULONG)buff[0] | ((ULONG)buff[1] << 8) | ((ULONG)buff[2] << 16) | ((ULONG)buff[3] << 24);
	if (dibsiz == 40) ftype = BITMAPINFOHEADER;
	else { f_close(bmpfil); return BR_DIB_ERR; }
	// Width and Height
	f_read(bmpfil, buff, 8, &dat);
	width = (ULONG)buff[0] | ((ULONG)buff[1] << 8) | ((ULONG)buff[2] << 16) | ((ULONG)buff[3] << 24);
	height = (ULONG)buff[4] | ((ULONG)buff[5] << 8) | ((ULONG)buff[6] << 16) | ((ULONG)buff[7] << 24);
	if(!((width == 320) && (height == 240))) { f_close(bmpfil); return BR_PAR_ERR; }
	f_read(bmpfil, buff, imgofs - 26, &dat);
	
	for (i = 0; i < 240; i++) {
		f_read(bmpfil, buff, 960, &dat);
		if (dat == 960) {
			for (j = 0; j < 320; j++) {
				linec[j] = RGB565(buff[j*3 + 2], buff[j*3 + 1], buff[j*3]);
			}
			GLCD_SetLine(239 - i, linec);
		} else {
			f_close(bmpfil);
			return BR_EOF;
		}
	}
	
	f_close(bmpfil);	
	return BR_OK;
	
}
