
/* ==============================================================
 @file: read file from SD Card use DOSFS Library written by Lewin A.R.W. Edwards (sysadm@zws.com).
www.larwe.com/zws/products/dosfs/index.html.
Read the TXT file then display the contents of the file on screen via USART1
Author:	 Pham Van Vang , HCM City University of Technology 
		Electronics and Telecommunication department

  ==============================================================*/
  


/*	
*************************************************************************************************************************************
									INCLUDED FILES															*
*************************************************************************************************************************************
*/
#include <stdio.h>
#include <main.h>
#include <diskio.h>
#include <dosfs.h>

  
/*
*************************************************************************************************************************************
*															PRIVATE DEFINE															*
*************************************************************************************************************************************
*/



/*
*************************************************************************************************************************************
*														 	DATA TYPE DEFINE															*
*************************************************************************************************************************************
*/

GPIO_InitTypeDef GPIO_InitStructure;
USART_InitTypeDef USART_InitStructure;

/*
*************************************************************************************************************************************
*													   		PRIVATE VARIABLES														*
*************************************************************************************************************************************
*/ 

uint8_t buff1[4096];
uint8_t sector[512];
uint16_t startclus , index;
uint32_t  pstart;
uint32_t  cache=0;
uint32_t j;
uint32_t  psize;
uint8_t ptype, pactive;
DIRINFO di;
DIRENT de;
VOLINFO vi;
/*
*************************************************************************************************************************************
*							  								LOCAL FUNCTIONS															*
*************************************************************************************************************************************
*/

/**
  * @brief  	Configures the different system clocks.
  * @param  	None
  * @retval 	None
  */
void RCC_Configuration(void)
{
	/* Setup the microcontroller system. Initialize the Embedded Flash Interface,  
	initialize the PLL and update the SystemFrequency variable. */
	//SystemInit();		// no need because SystemInit() is already called in start up code

	/* Enable GPIOA clocks */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB |RCC_APB2Periph_AFIO ,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);  
	
}


/**
  * @brief  	Inserts a delay time with resolution is 10 milisecond..
  * @param  	nCount: specifies the delay time length.
  * @retval 	None
  */
void Delay(__IO uint32_t num)
{
	__IO uint32_t index = 0;

	/* default system clock is 72MHz */
	for(index = (72000* num); index != 0; index--)
	{
	}
}

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
 PUTCHAR_PROTOTYPE
{
	/* Place your implementation of fputc here */
	USART_SendData(USART1, (uint8_t) ch);

	/* Loop until the end of transmission */
	while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
	{}

	return ch;
}
/*
*************************************************************************************************************************************
*															GLOBAL FUNCTIONS														*
*************************************************************************************************************************************
*/
/**
  * @brief  	Main program.
  * @param  	None
  * @retval 	None
  */
int status = 1;
int main(void) {
	
	/* Configure the system clocks */
	RCC_Configuration();	

	/* ===========================================================
	*	configure UART pins	
	============================================================*/

	/* Configure PA9 for USART Tx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Configure PA10 for USART Rx as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	/* USARTx configured as follow:
        - BaudRate = 115200 baud  
        - Word Length = 8 Bits
        - One Stop Bit
        - No parity
        - Hardware flow control disabled (RTS and CTS signals)
        - Receive and transmit enabled
  	*/
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	

	/* USART configuration */
	USART_Init(USART1, &USART_InitStructure);
	
	/* Enable USART1 */
	USART_Cmd(USART1 ,ENABLE) ;
		
	/* KHOI DONG CHUNG CHO BOARD*/
	power();

	/*==============================================
	  * khoi dong the nho
	  *value return = TRUE if secceed
	  			     FALSE if not                
	 ===============================================*/
	do
	{
		status = disk_initialize(0);
	}
	while(!status);
	/*==============================================
	 * Get partition 0 information 
	===============================================*/
	
	pstart = DFS_GetPtnStart(0, sector, 0, &pactive, &ptype,  &psize) ;
	
	if(pstart== 0xffffffff) {
		return 0;
	}
	
	/*======================================================
	  * Get volume information 
	  ======================================================*/
	if(DFS_GetVolInfo(0, sector, pstart, &vi)) {
		return 0;
	}
	
	/*=====================================================
		Get root directory 
	======================================================*/
	    
	if ( DFS_OpenDir(&vi, &di)) {
		return 0;
	}
		
	 /*======================================================
		  Get next entry in the root directory.
	  ======================================================*/ 
	index = 512;
	do {
		DFS_GetNext(&vi, &di, &de);
		index--;
	} while((de.name[8] != 'W') && index);	// wave format: file.wav

	if(!index)
	{
		printf("file.wav not found");
		printf("please create a *.wav in sd card for testing");
		return 0;
	}

	startclus= ( (uint16_t)de.startclus_l_l |((uint16_t)de.startclus_l_h )<< 8) ;

	j =  (pstart+ ((uint32_t)vi.reservedsecs) +(2*(vi.secperfat)) +((uint32_t)((vi.rootentries)*32/512 )) +(uint32_t)((startclus-2)*(vi.secperclus)));

	if (!disk_read(0, buff1, j, 4)) {
		return 0;
	}

	index = 0;
	while(index<512) {
		Delay(1);
		USART_SendData(USART1, buff1[index]);
		while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET) {}
		index++;
	}
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
	{
	}
}
#endif

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2009 ARMVietNam *****END OF FILE****/

