

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
/*-----------------------------------------------------------------------
/  Low level disk interface modlue include file  R0.07   (C)ChaN, 2009
/-----------------------------------------------------------------------*/

#ifndef _DISKIO


/* Results of Disk Functions */
typedef enum {
	RES_OK = 0,     /* 0: Successful */
	RES_ERROR,      /* 1: R/W Error */
	RES_WRPRT,      /* 2: Write Protected */
	RES_NOTRDY,     /* 3: Not Ready */
	RES_PARERR      /* 4: Invalid Parameter */
} DRESULT;


/*---------------------------------------*/
/* Prototypes for disk control functions */
void power (void);
uint8_t disk_initialize (uint8_t dr);
uint8_t disk_read (uint8_t dr , uint8_t *buff, uint32_t sector, uint8_t count);
uint8_t disk_write (uint8_t dr, const uint8_t *buff, uint32_t sector, uint8_t count);


#define _DISKIO
#endif
