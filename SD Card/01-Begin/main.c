#include "stm32f10x.h"
//#include "stm32f10x_spi.h"
//#include "stm32f10x_gpio.h"

#include "stdio.h"

#include "hpdragon_init.h"
#include "hpdragon_spi_sd.h"
#include "hpdragon_led.h"

/* Private macro -------------------------------------------------------------*/
#define countof(a) (sizeof(a) / sizeof(*(a)))

/* Private define ------------------------------------------------------------*/
#define BUFFER_SIZE (countof(TxBuffer)-1)

/* Private variables ---------------------------------------------------------*/
__IO uint16_t Status = 0;
uint8_t TxBuffer[] = "STM8S SPI Firmware Library Example: communication with a microSD card";
uint8_t RxBuffer[BUFFER_SIZE] = {0};
__IO ErrorStatus TransferStatus = ERROR;

static ErrorStatus Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint16_t BufferLength);

int main()
{
	int i = 0;
	
	LED_Init();
	
	//LED_OS_Port->ODR |= LED_OS_Pin;
	
	//init gpio, spi
	SD_LowLevel_Init();
	
	//init sd
	SD_Init();
	
	//LED_OS_Port->ODR &= ~LED_OS_Pin;
		
	//LED_OS_Port->ODR &= ~LED_OS_Pin;
	
	//SD_WriteBlock(TxBuffer, 0, BUFFER_SIZE);
	
	/* Write block of 512 bytes on address 0 */
	//SD_WriteBlock(TxBuffer, 0, BUFFER_SIZE);

	/* Read block of 512 bytes from address 0 */
	//SD_ReadBlock(RxBuffer, 0, BUFFER_SIZE);

	/* Check data */
	//TransferStatus = Buffercmp(TxBuffer, RxBuffer, BUFFER_SIZE);
	
	for(i = 0; i < 1000; i++);
	
	if (SD_ReadBlock(RxBuffer, 0, BUFFER_SIZE) == SD_RESPONSE_NO_ERROR)
		LED_OS_Port->ODR |= LED_OS_Pin;
	else
		LED_OS_Port->ODR &= ~LED_OS_Pin;

	
	while(1)
	{
	}
}

ErrorStatus Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint16_t BufferLength)
{
    while (BufferLength--)
    {
        if (*pBuffer1 != *pBuffer2)
        {
            return ERROR;
        }

        pBuffer1++;
        pBuffer2++;
    }

    return SUCCESS;
}

